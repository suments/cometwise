export default {
  APP_NAME: "DASHBOARD",

/////////////////////////////////////////
  APP_MY_SITES_TITLE: "Mis webs",
  APP_MY_SITES_TABLE_TITLE: "Resumen de mis webs",
  APP_MY_SITES_TABLE_COL_ID: "ID",
  APP_MY_SITES_TABLE_COL_PLAN: "Plan",
  APP_MY_SITES_TABLE_COL_DOMAIN: "Dominio",
  APP_MY_SITES_TABLE_COL_EXPIRY: "Válido hasta",
  APP_MY_SITES_TABLE_COL_TEXT_LEGAL: "Aviso legal",
  APP_MY_SITES_TABLE_COL_TEXT_COOKIES: "Aviso de cookies",
  APP_MY_SITES_TABLE_COL_TEXT_CGC: "Cond. Generales de contratación",
  APP_MY_SITES_TABLE_COL_TEXT_PRIVACY: "Aviso de privacidad",
  APP_MY_SITES_TABLE_COL_EDIT: "Editar",
  
  APP_MY_SITES_TABLE_PER_PAGE:"Webs por página",
  APP_MY_SITES_TABLE_PAGINATION_FROM:"Mostrando ",
  APP_MY_SITES_TABLE_PAGINATION_TO:"de",
  APP_MY_SITES_TABLE_PAGINATION_TOTAL:"De un total de",
/////////////////////////////////////////
  APP_ADD_SITE_BUTTON_LABEL: "Añadir nuevo sitio",

};
